# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0031_auto_20170306_1507'),
    ]

    operations = [
        migrations.AddField(
            model_name='useracreditation',
            name='contact_name_founderlist',
            field=models.CharField(help_text='contact name founderlist', max_length=1500, null=True, verbose_name='contact name founderlist', blank=True),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0012_auto_20161230_1446'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='phone',
            field=models.CharField(help_text='phone', max_length=50, null=True, verbose_name='Phone', blank=True),
        ),
    ]

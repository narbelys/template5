# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0029_profile_privacity'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='type_profile',
            field=models.CharField(default=1, help_text='Type of the profile', max_length=1, verbose_name='Type profile', choices=[(1, b'Startup'), (2, b'Inversionista')]),
            preserve_default=False,
        ),
    ]

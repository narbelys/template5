# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('all_page', '0034_profileinvest'),
    ]

    operations = [
        migrations.CreateModel(
            name='DataFilterEEAA',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data_filter', models.CharField(help_text='data filter', max_length=100, verbose_name='data filter')),
                ('type_filter', models.CharField(help_text='type filter', max_length=100, verbose_name='type filter')),
                ('name_filter', models.ForeignKey(to='all_page.Filter')),
            ],
        ),
        migrations.CreateModel(
            name='FilterEEAA',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text='name', max_length=100, verbose_name='name')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]

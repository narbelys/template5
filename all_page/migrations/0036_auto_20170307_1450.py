# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0035_datafiltereeaa_filtereeaa'),
    ]

    operations = [
        migrations.AlterField(
            model_name='datafiltereeaa',
            name='name_filter',
            field=models.ForeignKey(to='all_page.FilterEEAA'),
        ),
    ]

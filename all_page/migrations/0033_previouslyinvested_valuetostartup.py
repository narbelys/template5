# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('all_page', '0032_useracreditation_contact_name_founderlist'),
    ]

    operations = [
        migrations.CreateModel(
            name='PreviouslyInvested',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('previously_invested', models.CharField(help_text='Previously Invested', max_length=1, verbose_name='Previousy Invested', choices=[(1, b'Conocimiento en alguna industria en particular'), (2, b'Conexion con clientes y/o afiliados'), (3, b'Family Office'), (4, b'Direccion estrategica del negocio'), (5, b'Otros')])),
                ('other', models.CharField(help_text='other', max_length=1500, verbose_name='other')),
                ('user', models.ForeignKey(default=0, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='ValueToStartup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value_to_startup', models.CharField(help_text='Value To Startup', max_length=1, verbose_name='Value To Startup', choices=[(1, b'Directamente en una Startup'), (2, b'Directamente desde mi empresa'), (3, b'Desde un Family Office'), (4, b'No he invertido anteriormente')])),
                ('user', models.ForeignKey(default=0, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]

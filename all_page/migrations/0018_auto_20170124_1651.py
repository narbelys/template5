# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0017_auto_20170124_1647'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='search',
            field=models.IntegerField(help_text='Type of the option', verbose_name='search', choices=[(1, 'Buscando inversionista'), (2, 'Trabajando con FounderList')]),
        ),
    ]

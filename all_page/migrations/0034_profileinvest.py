# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('all_page', '0033_previouslyinvested_valuetostartup'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProfileInvest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('budget', models.IntegerField(null=True, verbose_name='Money', blank=True)),
                ('money', models.CharField(help_text='Type of the profile', max_length=1, verbose_name='Type profile', choices=[(1, b'USD $1k-4k')])),
                ('industry', models.ForeignKey(blank=True, to='all_page.Industry', null=True)),
                ('user', models.ForeignKey(default=0, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]

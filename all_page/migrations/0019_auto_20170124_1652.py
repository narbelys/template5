# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0018_auto_20170124_1651'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='logo',
            field=models.ImageField(upload_to=b'/home/augustoa/webapps/template_5_static/images', verbose_name=b'Logo', blank=True),
        ),
    ]

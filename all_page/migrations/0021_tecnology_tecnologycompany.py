# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0020_company_money'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tecnology',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text='name', max_length=100, verbose_name='name')),
            ],
        ),
        migrations.CreateModel(
            name='TecnologyCompany',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('company', models.ForeignKey(to='all_page.Company')),
                ('tecnology', models.ForeignKey(to='all_page.Place')),
            ],
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('all_page', '0004_auto_20161123_0226'),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('foto', models.ImageField(upload_to=b'', verbose_name=b'My Photo')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
        migrations.AlterField(
            model_name='survey',
            name='bci',
            field=models.NullBooleanField(help_text='client of BCI', verbose_name='Cliente BCI'),
        ),
        migrations.AlterField(
            model_name='survey',
            name='invest',
            field=models.NullBooleanField(help_text='invest', verbose_name='Aprende a invertir'),
        ),
        migrations.AlterField(
            model_name='survey',
            name='last_name',
            field=models.CharField(help_text='last name', max_length=1500, verbose_name='Apellido'),
        ),
        migrations.AlterField(
            model_name='survey',
            name='name',
            field=models.CharField(help_text='name', max_length=1500, verbose_name='Nombre'),
        ),
        migrations.AlterField(
            model_name='survey',
            name='sell_product',
            field=models.NullBooleanField(help_text='sell product', verbose_name='Ofrece producto'),
        ),
        migrations.AlterField(
            model_name='survey',
            name='sell_service',
            field=models.NullBooleanField(help_text='sell service', verbose_name='Ofrece servicio'),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('all_page', '0014_auto_20170106_1804'),
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('logo', models.ImageField(upload_to=b'/home/augustoa/webapps/template_5_static/images', verbose_name=b'Logo')),
                ('name', models.CharField(help_text='name', max_length=100, verbose_name='name')),
                ('description', models.CharField(help_text='description', max_length=1500, verbose_name='description')),
                ('web', models.CharField(help_text='web', max_length=1500, verbose_name='web')),
                ('search', models.CharField(help_text='Type of the option', max_length=1, verbose_name='search', choices=[(1, b'Buscando inversionista'), (2, b'Trabajando con FounderList')])),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, help_text='Date joined', verbose_name='date joined')),
            ],
        ),
        migrations.CreateModel(
            name='DataFilter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type_filter', models.CharField(help_text='Type of the filter', max_length=1, verbose_name='Type of filter', choices=[(1, b'Tipo'), (2, b'Locacion'), (3, b'Industria'), (4, b'Tecnologia'), (5, b'Equipo')])),
                ('data_filter', models.CharField(help_text='data filter', max_length=100, verbose_name='data filter')),
            ],
        ),
        migrations.CreateModel(
            name='Filter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text='name', max_length=100, verbose_name='name')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Industry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text='name', max_length=100, verbose_name='name')),
            ],
        ),
        migrations.CreateModel(
            name='Place',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text='name', max_length=100, verbose_name='name')),
            ],
        ),
        migrations.CreateModel(
            name='PlaceCompany',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('company', models.ForeignKey(to='all_page.Company')),
                ('place', models.ForeignKey(to='all_page.Place')),
            ],
        ),
        migrations.CreateModel(
            name='TypeCompany',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text='name', max_length=100, verbose_name='name')),
            ],
        ),
        migrations.AddField(
            model_name='datafilter',
            name='name_filter',
            field=models.ForeignKey(to='all_page.Filter'),
        ),
        migrations.AddField(
            model_name='company',
            name='industry',
            field=models.ForeignKey(to='all_page.Industry'),
        ),
        migrations.AddField(
            model_name='company',
            name='type_company',
            field=models.ForeignKey(to='all_page.TypeCompany'),
        ),
    ]

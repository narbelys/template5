# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2017-10-16 14:53
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0056_auto_20171016_1346'),
    ]

    operations = [
        migrations.CreateModel(
            name='EmployeeAll',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=150, null=True, verbose_name='description')),
                ('contacto', models.CharField(blank=True, max_length=150, null=True, verbose_name='contacto')),
                ('description', models.CharField(blank=True, max_length=1500, null=True, verbose_name='description')),
                ('startup', models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, to='all_page.Startup')),
            ],
        ),
    ]

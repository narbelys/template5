# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('all_page', '0030_profile_type_profile'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserAcreditation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('investment_fund', models.CharField(help_text='name', max_length=1500, null=True, verbose_name='name', blank=True)),
                ('name_contact', models.CharField(help_text='name contact', max_length=1500, null=True, verbose_name='name contact', blank=True)),
                ('contact_founderlist', models.BooleanField(default=False)),
                ('user', models.ForeignKey(default=0, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='UserTypeInvest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type_invest', models.CharField(help_text='Type of the invest', max_length=1, verbose_name='Type invest', choices=[(1, b'Individuo'), (2, b'Fondo de inversion'), (3, b'Family Office'), (4, b'Empresa')])),
                ('user', models.ForeignKey(default=0, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='profile',
            name='linkedin',
            field=models.CharField(max_length=1500, null=True, verbose_name='linkedin', blank=True),
        ),
    ]

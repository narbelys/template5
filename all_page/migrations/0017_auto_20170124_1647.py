# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0016_auto_20170118_1454'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='search',
            field=models.CharField(help_text='Type of the option', max_length=10, verbose_name='search', choices=[(1, b'Buscando inversionista'), (2, b'Trabajando con FounderList')]),
        ),
    ]

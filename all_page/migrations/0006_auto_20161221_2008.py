# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0005_auto_20161220_1944'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text='name', max_length=1500, verbose_name='Nombre')),
                ('date', models.CharField(help_text='date', max_length=1500, verbose_name='Date')),
                ('start_time', models.CharField(help_text='start_time', max_length=1500, verbose_name='start_time')),
                ('end_time', models.CharField(help_text='end_time', max_length=1500, null=True, verbose_name='end_time', blank=True)),
                ('place', models.CharField(help_text='place', max_length=1500, null=True, verbose_name='place', blank=True)),
                ('link', models.CharField(help_text='link', max_length=1500, null=True, verbose_name='link', blank=True)),
                ('description', models.CharField(help_text='description', max_length=1500, null=True, verbose_name='description', blank=True)),
                ('category', models.ForeignKey(help_text='category', to='all_page.Category')),
                ('city', models.ForeignKey(help_text='city', to='all_page.City')),
                ('country', models.ForeignKey(help_text='country', to='all_page.Country')),
            ],
        ),
        migrations.AddField(
            model_name='city',
            name='country',
            field=models.ForeignKey(help_text='country', to='all_page.Country'),
        ),
    ]

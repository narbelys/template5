# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='survey',
            name='bci',
            field=models.NullBooleanField(help_text='client of BCI', verbose_name='bci'),
        ),
        migrations.AlterField(
            model_name='survey',
            name='invest',
            field=models.NullBooleanField(help_text='invest', verbose_name='invest'),
        ),
        migrations.AlterField(
            model_name='survey',
            name='sell_product',
            field=models.NullBooleanField(help_text='sell product', verbose_name='sell_product'),
        ),
        migrations.AlterField(
            model_name='survey',
            name='sell_service',
            field=models.NullBooleanField(help_text='sell service', verbose_name='sell_service'),
        ),
        migrations.AlterField(
            model_name='survey',
            name='web',
            field=models.CharField(help_text='web', max_length=1500, null=True, verbose_name='web', blank=True),
        ),
    ]

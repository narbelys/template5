# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0028_auto_20170301_1606'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='privacity',
            field=models.BooleanField(default=True),
        ),
    ]

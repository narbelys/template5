# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0010_attend'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attend',
            name='event',
            field=models.ForeignKey(blank=True, to='all_page.Event', help_text='event', null=True),
        ),
    ]

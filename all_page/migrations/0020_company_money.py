# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0019_auto_20170124_1652'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='money',
            field=models.FloatField(default=0, help_text='Money', verbose_name='money'),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0021_tecnology_tecnologycompany'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tecnologycompany',
            name='tecnology',
            field=models.ForeignKey(to='all_page.Tecnology'),
        ),
    ]

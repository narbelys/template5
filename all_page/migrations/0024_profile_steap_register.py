# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0023_auto_20170130_1916'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='steap_register',
            field=models.IntegerField(default=1, help_text='Steap register', verbose_name='steap register'),
            preserve_default=False,
        ),
    ]

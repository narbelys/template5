# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0027_auto_20170301_1559'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='steap_register',
        ),
        migrations.AddField(
            model_name='profile',
            name='step_register',
            field=models.IntegerField(help_text='step register', null=True, verbose_name='step register', blank=True),
        ),
    ]

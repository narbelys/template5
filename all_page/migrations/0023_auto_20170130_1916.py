# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0022_auto_20170127_1911'),
    ]

    operations = [
        migrations.AlterField(
            model_name='datafilter',
            name='type_filter',
            field=models.CharField(help_text='type filter', max_length=100, verbose_name='type filter'),
        ),
    ]

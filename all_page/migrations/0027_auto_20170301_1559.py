# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0026_userinterest'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='description',
            field=models.CharField(max_length=1500, null=True, verbose_name='description', blank=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='steap_register',
            field=models.IntegerField(help_text='Steap register', null=True, verbose_name='steap register', blank=True),
        ),
    ]

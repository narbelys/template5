# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Information',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('choices_type', models.CharField(help_text='Type of the option', max_length=1, verbose_name='Type', choices=[(1, 'Ejecutivo'), (2, 'Mail'), (3, 'Facebook'), (4, 'Twitter'), (5, 'Referencia'), (6, 'Otra')])),
            ],
        ),
        migrations.CreateModel(
            name='SellService',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sell_type', models.CharField(help_text='Type of the option', max_length=1, verbose_name='sell_type', choices=[(1, 'Contaduria'), (2, 'Coaching'), (3, 'Disenio'), (4, 'Catering'), (5, 'Marketing'), (6, 'Desarrollo de software'), (7, 'Servicio al cliente'), (8, 'Recursos humanos'), (9, 'Legal'), (10, 'Ventas'), (11, 'Otro')])),
            ],
        ),
        migrations.CreateModel(
            name='Survey',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text='name', max_length=1500, verbose_name='name')),
                ('last_name', models.CharField(help_text='last name', max_length=1500, verbose_name='last_name')),
                ('mail', models.CharField(help_text='mail', unique=True, max_length=100, verbose_name='mail')),
                ('web', models.CharField(help_text='web', max_length=1500, verbose_name='web')),
                ('sell_service', models.BooleanField(help_text='sell service', verbose_name='sell_service')),
                ('sell_product', models.BooleanField(help_text='sell product', verbose_name='sell_product')),
                ('invest', models.BooleanField(help_text='invest', verbose_name='invest')),
                ('bci', models.BooleanField(help_text='client of BCI', verbose_name='bci')),
            ],
        ),
        migrations.AddField(
            model_name='sellservice',
            name='survey',
            field=models.ForeignKey(help_text='surveyl', to='all_page.Survey'),
        ),
        migrations.AddField(
            model_name='information',
            name='survey',
            field=models.ForeignKey(help_text='surveyl', to='all_page.Survey'),
        ),
    ]

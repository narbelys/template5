from django.contrib import admin
from .models import Survey, Information, SellService, Country, City, Event
from .models import Profile, Attend, TypeCompany, Industry, Company, Place
from .models import PlaceCompany, Filter, DataFilter, Category, Tecnology
from .models import TecnologyCompany, UserCity, UserInterest, UserTypeInvest
from .models import UserAcreditation, ProfileInvest, FilterEEAA, DataFilterEEAA, InvestIndustry
from .models import Startup, Ronda, Capitalizacion, Hitos, Product, Fundadores, Clientes
from .models import Competidores, Aliados, EmployeeAll, Directory, Advisor, Aceleradora
from .models import Inversiones, UserStartup
from django.contrib.auth.models import User


class InformationInline(admin.TabularInline):
    model = Information


class SellServiceInline(admin.TabularInline):
    model = SellService


class SurveyAdmin(admin.ModelAdmin):
    inlines = [InformationInline, SellServiceInline]


class TypeCompanyInline(admin.TabularInline):
    model = TypeCompany


class IndustryInline(admin.StackedInline):
    model = Industry


class PlaceCompanyInline(admin.StackedInline):
    model = PlaceCompany


#class CompanyAdmin(admin.ModelAdmin):
#    inlines = [IndustryInline, TypeCompanyInline]


admin.site.register(Survey, SurveyAdmin)
admin.site.register(Country)
admin.site.register(City)
admin.site.register(Event)
admin.site.register(Profile)
admin.site.register(Attend)
#admin.site.register(Company, CompanyAdmin)
admin.site.register(Company)
admin.site.register(Place)
admin.site.register(Filter)
admin.site.register(DataFilter)
admin.site.register(TypeCompany)
admin.site.register(Industry)
admin.site.register(Category)
admin.site.register(PlaceCompany)
admin.site.register(TecnologyCompany)
admin.site.register(Tecnology)
admin.site.register(UserCity)
admin.site.register(UserInterest)
admin.site.register(UserTypeInvest)
admin.site.register(UserAcreditation)
admin.site.register(ProfileInvest)
admin.site.register(FilterEEAA)
admin.site.register(DataFilterEEAA)
admin.site.register(InvestIndustry)
admin.site.register(Startup)
admin.site.register(Ronda)
admin.site.register(Capitalizacion)
admin.site.register(Hitos)
admin.site.register(Product)
admin.site.register(Fundadores)
admin.site.register(Clientes)
admin.site.register(Competidores)
admin.site.register(Aliados)
admin.site.register(EmployeeAll)
admin.site.register(Directory)
admin.site.register(Advisor)
admin.site.register(Aceleradora)
admin.site.register(Inversiones)
admin.site.register(UserStartup)
# Register your models here.

# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.shortcuts import get_object_or_404, render_to_response, redirect
from django.template import RequestContext
from django.contrib.auth import logout as django_logout
from django.contrib.auth import authenticate
from django.http import HttpResponseRedirect, HttpResponse,\
                        HttpResponseNotAllowed, HttpResponseBadRequest, \
                        HttpResponseForbidden
from django.core.mail import send_mail
from django.conf import settings
from all_page.models import Survey, Information, SellService, Country, City
from all_page.models import Profile, UserCity
from all_page.models import Category, Event, Attend, Company, TypeCompany
from all_page.models import Place, PlaceCompany, Industry, Tecnology
from all_page.models import TecnologyCompany, Filter, DataFilter, UserInterest
from all_page.models import FilterEEAA, DataFilterEEAA
from all_page.models import UserTypeInvest, UserAcreditation, ValueToStartup
from all_page.models import PreviouslyInvested, ProfileInvest, InvestIndustry, Startup
from all_page.models import Ronda, Capitalizacion, Hitos, Product, Fundadores, Clientes
from all_page.models import Competidores, Aliados, EmployeeAll, Directory, Advisor
from all_page.models import Aceleradora, Inversiones, UserStartup
from django.db.models import Count
from django.core import serializers
from django.contrib.auth.models import User
from collections import defaultdict
import math
from django.views.decorators.csrf import ensure_csrf_cookie
import json
import datetime
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
import string
import random


def login(request):
    template_name = 'login.html'
    if request.method == 'POST':
        email = request.POST.get('email', None)
        password = request.POST.get('password', None)
        print email, password
        users = User.objects.filter(email=email)
        if users:
            username = users[0].username
        else:
            username = ''
        user = authenticate(username=username, password=password)
        print user
        if user is None:
            return render_to_response(template_name, {"message": "Error de autenticación clave o usuario incorrectos"},
                                      context_instance=RequestContext(request))
        else:
            auth_login(request, user)
            if Profile.objects.filter(user=user, type_profile=2):
                return redirect("/profile_investor")
            else:
                return redirect("/profile_startup")
    else:
        return render_to_response(template_name, {},
                                  context_instance=RequestContext(request))

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def recover(request):
    template_name = 'password_recover.html'
    if request.method == 'POST':
        email = request.POST.get('email', None)
        #try:
        u = User.objects.get(email=email)
        generator = id_generator()
        u.set_password(generator)
        u.save()
        send_mail('Recuperar password', 'Su nuevo password es: '+ generator, 'narbelys@brookesia.cl',[email], fail_silently=False)
        return render_to_response('confirmation_recover.html', {},
                                  context_instance=RequestContext(request))
       # except:
        #    return render_to_response(template_name, {'message': "Correo no registrado en el sistema"},
        #                          context_instance=RequestContext(request))
    else:
        return render_to_response(template_name, {},
                                  context_instance=RequestContext(request))


def register(request):
    template_name = 'register.html'
    data = {}
    step = 1
    if request.method == 'POST':
        email = request.POST.get('email', None)
        name = request.POST.get('name', None)
        last_name = request.POST.get('last_name', None)
        password_1 = request.POST.get('password_1', None)
        password_2 = request.POST.get('password_2', None)
        if password_1 != password_2:
            message = "Claves no coinciden"
        else:
            try:
                if User.objects.filter(username=email):
                    message = "Usuario ya registrado"
                else:
                    user = User.objects.create_user(username=email,
                                                    email=email,
                                                    first_name=name,
                                                    last_name=last_name,
                                                    password=password_1)
                    user = authenticate(username=email, password=password_1)
                    profile = Profile(user=user, type_profile=1,
                                      step_register=1)
                    profile.save()
                    auth_login(request, user)
                    return redirect("/register_location")
                    message = """Registro de usuario de manera exitosa al
                                segundo paso"""
            except:
                message = "Error en el registro"
        data['message'] = message
        return render_to_response(template_name, data,
                                  context_instance=RequestContext(request))
    else:
        return render_to_response(template_name, {'step': step},
                                  context_instance=RequestContext(request))


def register_location(request):
    template_name = 'register.html'
    data = {}
    step = 2
    progress = 16
    city = City.objects.all().values_list('name', 'country__name')
    country_city = []
    for i in city:
        country_city.append(i[0]+', '+i[1])
    if request.method == 'POST':
        profile = Profile.objects.get(user=request.user)
        profile.step_register = 2
        location = request.POST.get('location', None)
        try:
            location = location.split('-')
            for i in location:
                location_array = i.split(',')
                city = City.objects.get(
                    name=location_array[0].strip(),
                    country__name=location_array[1].strip())
                user_city = UserCity(user=request.user, city=city)
                user_city.save()
            message = "Paso dos con exito"
            return redirect("/register_interests")
        except:
            message = "Error en el registro"
        data['message'] = message
        return render_to_response(template_name, data,
                                  context_instance=RequestContext(request))
    else:
        return render_to_response(template_name, {'step': step,
                                                  'city': country_city,
                                                  'progress': progress},
                                  context_instance=RequestContext(request))


def register_interests(request):
    template_name = 'register.html'
    data = {}
    step = 3
    progress = 32
    city = City.objects.all().values_list('name', 'country__name')
    country_city = []
    for i in city:
        country_city.append(i[0]+', '+i[1])
    if request.method == 'POST':
        profile = Profile.objects.get(user=request.user)
        profile.step_register = 3
        people = request.POST.getlist('people')
        company = request.POST.getlist('company')
        try:
            for i in people:
                user_interest = UserInterest(user=request.user,
                                             interest=i,
                                             type_interest='people')
                user_interest.save()
            for i in company:
                user_interest = UserInterest(user=request.user,
                                             interest=i,
                                             type_interest='company')
                user_interest.save()
            message = "Paso tres con exito"
            return redirect("/register_avatar")
        except:
            message = "Error en el registro"
        data['message'] = message
        return render_to_response(template_name, data,
                                  context_instance=RequestContext(request))
    else:
        return render_to_response(template_name, {'step': step,
                                                  'city': country_city,
                                                  'progress': progress},
                                  context_instance=RequestContext(request))


def register_avatar(request):
    template_name = 'register.html'
    data = {}
    step = 4
    progress = 48
    if request.method == 'POST':
        avatar = request.POST.get('avatar_text', None)
        try:
            if 'avatar' in request.FILES:
                profile = Profile.objects.get(user=request.user)
                profile.foto = request.FILES['avatar']
                profile.step_register = 4
                profile.save()
                message = "Paso cuatro con exito"
            return redirect("/register_mini_resume")
        except:
            message = "Error en el registro"
            return redirect("/register_mini_resume")
        data['message'] = message
        return render_to_response(template_name, data,
                                  context_instance=RequestContext(request))
    else:
        return render_to_response(template_name, {'step': step,
                                                  'progress': progress},
                                  context_instance=RequestContext(request))


def register_mini_resume(request):
    template_name = 'register.html'
    data = {}
    step = 5
    progress = 64
    if request.method == 'POST':
        mini_resume = request.POST.get('mini_resume', None)
        try:
            profile = Profile.objects.get(user=request.user)
            profile.description = mini_resume
            profile.step_register = 5
            profile.save()
            message = "Paso cinco con exito"
            return redirect("/register_privacy")
        except:
            message = "Error en el registro"
        data['message'] = message
        return render_to_response(template_name, data,
                                  context_instance=RequestContext(request))
    else:
        return render_to_response(template_name, {'step': step,
                                                  'progress': progress},
                                  context_instance=RequestContext(request))


def register_privacy(request):
    template_name = 'register.html'
    data = {}
    step = 6
    progress = 80
    profile = Profile.objects.get(user=request.user)
    cities = UserCity.objects.filter(user=request.user)
    if request.method == 'POST':
        mode = request.POST.get('mode', None)
        try:
            if mode == 'public':
                profile.privacity = False
            else:
                profile.privacity = True
            profile.step_register = 6
            profile.save()
            message = "Paso seis con exito"
            return redirect("/profile_startup")
        except:
            message = "Error en el registro"
        data['message'] = message
        return render_to_response(template_name, data,
                                  context_instance=RequestContext(request))
    else:
        return render_to_response(template_name, {'step': step,
                                                  'profile': profile,
                                                  'user': request.user,
                                                  'cities': cities,
                                                  'progress': progress},
                                  context_instance=RequestContext(request))


def logout(request):
    django_logout(request)
    return redirect('home')


def index(request):
    template_name = 'index.html'
    if request.method == 'POST':
        if 'contact-name' in request.POST:
            name = request.POST.get('contact-name', None)
            mail = request.POST.get('contact-email', None)
            message = request.POST.get('contact-message', None)
            try:
                send_mail('Contacto y Dudas Founderlist',
                          'Realizaron una consulta en Founderlist:\n Nombre Completo: '+ name + '\n Email: ' + mail + '\n Mensaje: ' + message, 'founderlist@teamussolutions.com',['hello@founderlist.la'], fail_silently=False)
                return render_to_response(template_name, {'message':'Mensaje enviado'},
                                          context_instance=RequestContext(request))
            except:
                return render_to_response(template_name, {'message':'Error al enviar el mensaje'},
                                          context_instance=RequestContext(request))
        else:
            name = request.POST.get('name', None)
            last_name = request.POST.get('last_name', None)
            mail = request.POST.get('mail', None)
            phone = request.POST.get('phone', None)
            web = request.POST.get('web', None)
            sell_service = request.POST.getlist('sell_service[]')
            bci = request.POST.get('bci', None)
            information = request.POST.getlist('information[]')
            try:
                if Survey.objects.filter(mail=mail):
                    return render_to_response(template_name, {'message':'Correo ya registrado'},
                                              context_instance=RequestContext(request))
                if bci == 1:
                    survey = Survey(name=name,
                                    last_name=last_name,
                                    mail=mail, phone=phone, web=web, bci=True)
                else:
                    survey = Survey(name=name,
                                    last_name=last_name,
                                    mail=mail, phone=phone, web=web, bci=False)
                survey.save()
                if sell_service:
                    survey.sell_service = True
                    for i in sell_service:
                        sell_service = SellService(survey=survey,
                                                   sell_type=int(i))
                        sell_service.save()
                else:
                    survey.sell_service = False
                if 'sell_product' in request.POST:
                    survey.sell_product = True
                else:
                    survey.sell_product = False

                if 'invest' in request.POST:
                    survey.invest = True
                else:
                    survey.invest = False
                survey.save()

                for i in information:
                    information = Information(survey=survey, origin=int(i))
                    information.save()
                return render_to_response(template_name, {'message': u"Su registro al evento ha sido exitoso, pronto el equipo FounderList se contactará contigo"},
                                          context_instance=RequestContext(request))
            except:
                return render_to_response(template_name, {'message': u'Su registro al evento ha sido exitoso, pronto el equipo FounderList se contactará contigo'},
                                          context_instance=RequestContext(request))

#


    else:
        return render_to_response(template_name, {},
                                  context_instance=RequestContext(request))


def home(request):
    template_name = 'home.html'
    return render_to_response(template_name, {},
                              context_instance=RequestContext(request))


def register_investor(request):
    template_name = 'register_investor/home.html'
    return render_to_response(template_name, {},
                              context_instance=RequestContext(request))


def register_investor_s1(request):
    template_name = 'register_investor/register.html'
    data = {}
    step = 1
    if request.method == 'POST':
        email = request.POST.get('email', None)
        name = request.POST.get('name', None)
        last_name = request.POST.get('last_name', None)
        password_1 = request.POST.get('password_1', None)
        password_2 = request.POST.get('password_2', None)
        if password_1 != password_2:
            message = "Claves no coinciden"
        else:
            try:
                if User.objects.filter(username=email):
                    message = "Usuario ya registrado"
                else:
                    user = User.objects.create_user(username=email,
                                                    email=email,
                                                    first_name=name,
                                                    last_name=last_name,
                                                    password=password_1)
                    user = authenticate(username=email, password=password_1)
                    profile = Profile(user=user,
                                      type_profile=2,
                                      step_register=1)
                    profile.save()
                    auth_login(request, user)
                    return redirect("/home/register_investor_s2")
                    message = """Registro de usuario de manera exitosa al
                                segundo paso"""
            except:
                message = "Error en el registro"
        data['message'] = message
        return render_to_response(template_name, data,
                                  context_instance=RequestContext(request))
    else:
        return render_to_response(template_name, {'step': step},
                                  context_instance=RequestContext(request))


def register_investor_s2(request):
    template_name = 'register_investor/register.html'
    data = {}
    step = 2
    progress = 25
    if request.method == 'POST':
        invest_type = request.POST.getlist('invest_type')
        investment_fund = request.POST.get('investment_fund', None)
        name_contact = request.POST.get('name_contact', None)
        web = request.POST.get('web', None)
        contact_name_founderlist = request.POST.get('contact_name_founderlist',
                                                    None)
        linkedin = request.POST.get('linkedin', None)
        contact_founderlist = request.POST.get('contact_founderlist', None)
        #first_name = request.POST.get('first_name', None)
        #last_name = request.POST.get('last_name', None)
        print contact_founderlist
        if contact_founderlist == "True":
            contact_founderlist = True
        else:
            contact_founderlist = False
        for i in invest_type:
            user_type_invest = UserTypeInvest(user=request.user, type_invest=i)
            user_type_invest.save()
        user_acreditation = UserAcreditation(
                            user=request.user,
                            investment_fund=investment_fund,
                            name_contact=name_contact,
                            contact_founderlist=contact_founderlist,
                            web=web)
        user_acreditation.save()
        if contact_name_founderlist:
            user_acreditation.contact_name_founderlist = (
                                                contact_name_founderlist
                                                        )
            user_acreditation.save()
        profile = Profile.objects.get(user=request.user)
        profile.linkedin = linkedin
        profile.save()
        return redirect("/home/register_investor_s3")
    else:
        return render_to_response(template_name, {'step': step,
                                                  'progress': progress},
                                  context_instance=RequestContext(request))


def register_investor_s3(request):
    template_name = 'register_investor/register.html'
    data = {}
    step = 3
    progress = 50
    if request.method == 'POST':
        value_to_startup = request.POST.getlist('value_to_startup')
        previously_invested = request.POST.getlist('previously_invested')
        other_text = request.POST.get('other_text', None)

        for i in value_to_startup:
            startup_user = ValueToStartup(user=request.user,
                                          value_to_startup=i)
            startup_user.save()
        for i in previously_invested:
            previously_invested = PreviouslyInvested(
                                    user=request.user,
                                    previously_invested=i)
            if i == 5 and other_text:
                previously_invested.other = other_text
            previously_invested.save()

        return redirect("/home/register_investor_s4")
    else:
        return render_to_response(template_name, {'step': step,
                                                  'progress': progress},
                                  context_instance=RequestContext(request))


def register_investor_s4(request):
    template_name = 'register_investor/register.html'
    data = {}
    step = 4
    progress = 75
    industries = Industry.objects.all().values_list('name', flat=True)
    city = City.objects.all().values_list('name', flat=True)
    country = Country.objects.all().values_list('name', flat=True)
    #country_city = []
    #for i in city:
    #    country_city.append(i[0]+', '+i[1])
    if request.method == 'POST':
        location = request.POST.get('location', None)
        country = request.POST.get('country', None)
        budget = request.POST.get('budget', None)
        money = request.POST.get('money', None)
        industry = request.POST.get('industry', None)
        description = request.POST.get('description', None)
        #try:
        location = location.split(',')
        location = location[0]
        country = country.split(',')
        country = country[0]
        city = City.objects.get(
            name=location,
            country__name=country)
        user_city = UserCity(user=request.user, city=city)
        user_city.save()

        profile = Profile(user=request.user)
        profile.description = description
        profile.save()
        #industry = Industry.objects.get(id=industry)
        profile_invest = ProfileInvest(user=request.user,
                                       budget=int(budget),
                                       money=money)
        profile_invest.save()
        industry = industry.split(',')
        for i in industry:
            industry_element = Industry.objects.get(
                name=i)
            invest_industry = InvestIndustry(industry= industry_element, investor=profile_invest)
            invest_industry.save()
        message = "Paso dos con exito"
        return redirect("/profile_investor/")
        #except:
        #    message = "Error en el registro"

        return redirect("/profile_investor")
    else:
        return render_to_response(template_name, {
                                'step': step,
                                'industries': industries,
                                'city': city,
                                'country': country,
                                'progress': progress},
                                  context_instance=RequestContext(request))

def profile_invest(request):
    template_name = 'profile_investor/profile_investor.html'
    data = {}
    if request.method == 'POST':
        if 'username' in request.POST: #entró a editar el banner 
            save_banner(request=request) 
        elif 'budget' in request.POST: #preferencias de inversión
            print '---entro'
            save_money_investor(request=request)
            return redirect("/profile_investor/#investments-section") 
                  

    data['user_type_invest'] = UserTypeInvest.objects.get(user=request.user).get_type_invest_display()
    data['profile'] = Profile.objects.filter(user=request.user)[0]
    data['user_city'] = UserCity.objects.filter(user=request.user)[0]
    city = City.objects.all()
    data['cities'] = []
    for i in city:
        data['cities'].append(i.get_all_name())
    data['profile_invest'] = ProfileInvest.objects.filter(user=request.user)[0]
    data['industries'] = Industry.objects.all()
    data['user_industry'] = InvestIndustry.objects.filter(investor=data['profile_invest'])[0]     
    return render_to_response(template_name, data,
                              context_instance=RequestContext(request))

def profile_startup(request, name_startup=None):
    data = {}
    data['user'] = request.user
    data['profile'] = Profile.objects.get(user=request.user)
    data['startup'] = Startup.objects.filter(user=request.user)
    if name_startup:
       data['startup'] = Startup.objects.filter(name_index=name_startup)
       data['propietario'] = False
       user_startup = UserStartup.objects.filter(user=request.user)
       if not user_startup:
        user_startup = UserStartup(user=request.user, startup=data['startup'][0])
        user_startup.save()
    else: 
        data['propietario'] = True
        data['user_startup'] = UserStartup.objects.filter(startup=data['startup'][0])
    if data['startup']:
        data['startup'] = data['startup'][0]
        data['product'] = Product.objects.filter(startup=data['startup'])
        data['founders'] = Fundadores.objects.filter(startup=data['startup'])
        data['clientes'] = Clientes.objects.filter(startup=data['startup'])
        data['aliados'] = Aliados.objects.filter(startup=data['startup'])
        data['employees'] = EmployeeAll.objects.filter(startup=data['startup'])
        data['directory'] = Directory.objects.filter(startup=data['startup'])
        data['advisors'] = Advisor.objects.filter(startup=data['startup'])
        data['aceleradoras'] = Aceleradora.objects.filter(startup=data['startup'])
        data['inversiones'] = Inversiones.objects.filter(startup=data['startup'])
        data['competidores'] = Competidores.objects.filter(startup=data['startup'])
        data['ronda'] = Ronda.objects.filter(startup=data['startup'])
        if data['ronda']:
            data['ronda'] = data['ronda'][0]
            data['capitalizacion'] = Capitalizacion.objects.filter(ronda=data['ronda'])
            data['hitos'] = Hitos.objects.filter(ronda=data['ronda'])
        else:
            data['ronda'] = None
    else:
        data['startup'] = None
        data['ronda'] = None    
    

    data['user_city'] = UserCity.objects.filter(user=request.user)[0]
    city = City.objects.all()
    data['cities'] = []
    for i in city:
        data['cities'].append(i.get_all_name())
    template_name = 'profile_startup/profile_startup.html'
    print request
    if request.method == 'POST':
        print request.POST
        profile = Startup.objects.get(user=request.user)
        if 'name' in request.POST: #entró a editar el banner 
            save_banner_startup(request=request) 
            return redirect("/profile_startup/")        
        if 'name_index' in request.POST:
            startup = data['startup']
            startup.name_index = request.POST.get('name_index', None)  
            startup.save()
            return redirect("/profile_startup/")
        elif 'valorizacion' in request.POST:
            save_ronda(request=request) 
            return redirect("/profile_startup/")        
        elif 'description_product' in request.POST:
            save_product(request=request) 
            return redirect("/profile_startup/")        
        elif 'name_founder' in request.POST:
            save_founder(request=request) 
            return redirect("/profile_startup/")        
        elif 'id_founder_delete' in request.POST:
            delete_founder(request=request) 
            print 'entro por founder'
            return redirect("/profile_startup/")        
        elif 'who_founder' in request.POST:
            save_description_founder(request=request) 
            return redirect("/profile_startup/")
        elif 'name_client' in request.POST:
            save_client(request=request) 
            return redirect("/profile_startup/")              
        elif 'id_client_delete' in request.POST:
            delete_aliado(request=request) 
            return redirect("/profile_startup/")  
        elif 'id_aliado_delete' in request.POST:
            delete_client(request=request) 
            return redirect("/profile_startup/")              
        elif 'name_aliado' in request.POST:
            save_aliados(request=request) 
            return redirect("/profile_startup/")          
        elif 'name_employee' in request.POST:
            save_employee(request=request) 
            return redirect("/profile_startup/")        
        elif 'id_employee_delete' in request.POST:
            delete_employee(request=request) 
            return redirect("/profile_startup/")          
        elif 'name_directory' in request.POST:
            save_directory(request=request) 
            return redirect("/profile_startup/")        
        elif 'id_directory_delete' in request.POST:
            delete_directory(request=request) 
            return redirect("/profile_startup/")          
        elif 'name_advisor' in request.POST:
            save_advisor(request=request) 
            return redirect("/profile_startup/")        
        elif 'id_advisor_delete' in request.POST:
            delete_advisor(request=request) 
            return redirect("/profile_startup/")         
        elif 'name_aceleradora' in request.POST:
            save_aceleradora(request=request) 
            return redirect("/profile_startup/")        
        elif 'id_aceleradora_delete' in request.POST:
            delete_aceleradora(request=request) 
            return redirect("/profile_startup/")         
        elif 'money_inversiones' in request.POST:
            save_inversiones(request=request) 
            return redirect("/profile_startup/")        
        elif 'id_inversiones_delete' in request.POST:
            delete_inversiones(request=request) 
            return redirect("/profile_startup/")                 
        elif 'ingreso_model' in request.POST:
            profile.ingreso_model = request.POST.get('ingreso_model', None)  
            profile.save()
            return redirect("/profile_startup/")  
        elif 'client_number' in request.POST:
            save_transaction(request=request) 
            return redirect("/profile_startup/")          
        elif 'chanel_distribution' in request.POST:
            profile.chanel_distribution = request.POST.get('chanel_distribution', None)  
            profile.save()
            return redirect("/profile_startup/")          
        elif 'mercado' in request.POST:
            profile.mercado = request.POST.get('mercado', None)  
            profile.save()
            return redirect("/profile_startup/")          
        elif 'competidores' in request.POST:
            save_competidores(request=request) 
            return redirect("/profile_startup/")          
    return render_to_response(template_name, data,
                              context_instance=RequestContext(request))
def save_banner_startup(request):
    profile = Startup.objects.filter(user=request.user) 
    city = request.POST.get('city', None) 
    city = city.split(' , ') 
    city = City.objects.get(name=city[0], country__name=city[1])
    name = request.POST.get('name', None) 
    type_bussines = request.POST.get('type_bussines', None) 
    employee = request.POST.get('employee', None) 
    twitter = request.POST.get('twitter', None) 
    facebook = request.POST.get('facebook', None) 
    blog = request.POST.get('blog', None) 
    linkedin = request.POST.get('linkedin', None) 
    web_page = request.POST.get('web_page', None) 
    description = request.POST.get('mini_resume', None) 
    if profile:
        profile = profile[0]
        profile.city = city
        profile.name = name
        profile.type_bussines = type_bussines
        profile.employee = employee
        profile.twitter = twitter
        profile.facebook = facebook
        profile.blog = blog
        profile.linkedin = linkedin
        profile.web_page = web_page
        profile.description = description
        profile.save()
    else:
        profile = Startup(user=request.user, city=city, name=name, type_bussines=type_bussines, 
                          twitter=twitter, facebook=facebook, blog=blog, 
                          linkedin=linkedin, description=description, employee=employee,
                          web_page=web_page)
        profile.save()
def save_ronda(request):
    profile = Startup.objects.filter(user=request.user) 
    size = request.POST.get('size', None) 
    search_money = request.POST.get('search_money', None) 
    participacion = request.POST.get('participacion', None) 
    valorizacion = request.POST.get('valorizacion', None) 
    other_term = request.POST.get('other_term', None) 
    equity = json.loads(request.POST.get('equity', None)) 
    if profile:
        profile = profile[0]
    else:
        profile = Startup(user=request.user)
        profile.save()
    ronda = Ronda.objects.filter(startup=profile) 
    if ronda:
        ronda = ronda[0]
        ronda.size = size
        ronda.search_money = search_money
        ronda.participacion = participacion
        ronda.valorizacion = valorizacion
        ronda.other_term = other_term
        ronda.equity = equity
        ronda.save()
    else:
        ronda = Ronda(startup=profile, size=size, search_money=search_money, 
                      participacion=participacion, valorizacion=valorizacion, 
                      other_term=other_term, equity=equity)
        ronda.save()
    capitalizaciones = request.POST.get('capitalizaciones', None) 
    capitalizacion = Capitalizacion.objects.filter(ronda=ronda)
    capitalizacion.delete()
    for i in range(int(capitalizaciones)):
         name_capitalizacion = request.POST.get('name_capitalizacion_'+str(i+1), None) 
         type_paticipacion = request.POST.get('type_paticipacion_'+str(i+1), None) 
         participacion = request.POST.get('participacion_'+str(i+1), None) 
         if name_capitalizacion:
            capitalizacion = Capitalizacion(name_capitalizacion=name_capitalizacion, 
                                            type_paticipacion=type_paticipacion,
                                            participacion=participacion, ronda=ronda)
            capitalizacion.save()    
    hitos = request.POST.get('hitos', None) 
    hito = Hitos.objects.filter(ronda=ronda)
    hito.delete()
    for i in range(int(hitos)):
         print i
         hitos_name = request.POST.get('hitos_'+str(i+1), None) 
         activities = request.POST.get('activities_'+str(i+1), None) 
         time = request.POST.get('time_'+str(i+1), None) 
         money = request.POST.get('money_'+str(i+1), None) 
         print hitos_name
         if hitos_name:
            hito_obj = Hitos(hitos=hitos_name, 
                          activities=type_paticipacion,
                          time=participacion, 
                          money=money,
                          ronda=ronda)
            hito_obj.save()

def save_product(request):
    profile = Startup.objects.get(user=request.user) 
    product = Product.objects.filter(startup=profile) 
    description_product = request.POST.get('description_product', None) 
    video_product = request.POST.get('video_product', None) 
    if product:
        product = product[0]
        product.description = description_product 
        product.video = video_product 
        product.save()
    else:
        product = Product(description=description_product, video=video_product, startup=profile)
        product.save()

def save_founder(request):
    profile = Startup.objects.get(user=request.user) 
    name = request.POST.get('name_founder', None) 
    cargo = request.POST.get('cargo_founder', None) 
    tiempo = request.POST.get('dedicacion_founder', None) 
    description = request.POST.get('description_founder', None) 
    id_founder = request.POST.get('id_founder', None) 
    if id_founder!="":
        founder = Fundadores.objects.get(pk=int(id_founder)) 
        founder.name = name 
        founder.cargo = cargo 
        founder.tiempo = tiempo 
        founder.description = description 
        founder.save()
    else:
        founder = Fundadores(name=name, cargo=cargo, tiempo=tiempo, description=description, startup=profile)
        founder.save()

def delete_founder(request):
    id_founder = request.POST.get('id_founder_delete', None) 
    founder = Fundadores.objects.get(pk=int(id_founder)) 
    founder.delete()

def save_description_founder(request):
    profile = Startup.objects.get(user=request.user) 
    url = request.POST.get('url_founder', None) 
    who_are = request.POST.get('who_founder', None) 
    why = request.POST.get('why_founder', None) 
    profile.video = url
    profile.who_are = who_are
    profile.why = why
    profile.save()

def save_client(request):
    profile = Startup.objects.get(user=request.user) 
    name = request.POST.get('name_client', None) 
    cargo = request.POST.get('type_client', None) 
    description = request.POST.get('description_client', None) 
    id_client = request.POST.get('id_client', None) 
    if id_client!="":
        founder = Clientes.objects.get(pk=int(id_client)) 
        founder.name = name 
        founder.contacto = cargo  
        founder.description = description 
        founder.save()
    else:
        founder = Clientes(name=name, contacto=cargo, description=description, startup=profile)
        founder.save()

def delete_client(request):
    id_client = request.POST.get('id_aliado_delete', None) 
    client = Aliados.objects.get(pk=int(id_client)) 
    client.delete()

def save_aliados(request):
    profile = Startup.objects.get(user=request.user) 
    name = request.POST.get('name_aliado', None) 
    cargo = request.POST.get('type_aliado', None) 
    description = request.POST.get('description_aliado', None) 
    id_aliado = request.POST.get('id_aliado', None) 
    if id_aliado!="":
        founder = Aliados.objects.get(pk=int(id_aliado)) 
        founder.name = name 
        founder.contacto = cargo  
        founder.description = description 
        founder.save()
    else:
        founder = Aliados(name=name, contacto=cargo, description=description, startup=profile)
        founder.save()

def delete_aliado(request):
    id_aliado = request.POST.get('id_aliado_delete', None) 
    aliado = Aliados.objects.get(pk=int(id_aliado)) 
    aliado.delete()

def save_employee(request):
    print request.POST
    profile = Startup.objects.get(user=request.user) 
    name = request.POST.get('name_employee', None) 
    cargo = request.POST.get('type_employee', None) 
    description = request.POST.get('description_employee', None) 
    id_employee = request.POST.get('id_employee', None) 
    if id_employee!="":
        employee = EmployeeAll.objects.get(pk=int(id_employee)) 
        employee.name = name 
        employee.contacto = cargo  
        employee.description = description 
        employee.save()
    else:
        employee = EmployeeAll(name=name, contacto=cargo, description=description, startup=profile)
        employee.save()

def delete_employee(request):
    id_employee = request.POST.get('id_employee_delete', None) 
    employee = EmployeeAll.objects.get(pk=int(id_employee)) 
    employee.delete()

def save_directory(request):
    print request.POST
    profile = Startup.objects.get(user=request.user) 
    name = request.POST.get('name_directory', None) 
    cargo = request.POST.get('type_directory', None) 
    description = request.POST.get('description_directory', None) 
    id_directory = request.POST.get('id_directory', None) 
    if id_directory!="":
        directory = Directory.objects.get(pk=int(id_directory)) 
        directory.name = name 
        directory.contacto = cargo  
        directory.description = description 
        directory.save()
    else:
        directory = Directory(name=name, contacto=cargo, description=description, startup=profile)
        directory.save()

def delete_directory(request):
    id_directory = request.POST.get('id_directory_delete', None) 
    directory = Directory.objects.get(pk=int(id_directory)) 
    directory.delete()

def save_advisor(request):
    print request.POST
    profile = Startup.objects.get(user=request.user) 
    name = request.POST.get('name_advisor', None) 
    cargo = request.POST.get('type_advisor', None) 
    description = request.POST.get('description_advisor', None) 
    id_advisor = request.POST.get('id_advisor', None) 
    if id_advisor!="":
        advisor = Advisor.objects.get(pk=int(id_advisor)) 
        advisor.name = name 
        advisor.contacto = cargo  
        advisor.description = description 
        advisor.save()
    else:
        advisor = Advisor(name=name, contacto=cargo, description=description, startup=profile)
        advisor.save()

def delete_advisor(request):
    id_advisor = request.POST.get('id_advisor_delete', None) 
    advisor = Advisor.objects.get(pk=int(id_advisor)) 
    advisor.delete()

def save_aceleradora(request):
    profile = Startup.objects.get(user=request.user) 
    name = request.POST.get('name_aceleradora', None) 
    cargo = request.POST.get('type_aceleradora', None) 
    description = request.POST.get('description_aceleradora', None) 
    id_aceleradora = request.POST.get('id_aceleradora', None) 
    if id_aceleradora!="":
        aceleradora = Aceleradora.objects.get(pk=int(id_aceleradora)) 
        aceleradora.name = name 
        aceleradora.contacto = cargo  
        aceleradora.description = description 
        aceleradora.save()
    else:
        aceleradora = Aceleradora(name=name, contacto=cargo, description=description, startup=profile)
        aceleradora.save()

def delete_aceleradora(request):
    id_aceleradora = request.POST.get('id_aceleradora_delete', None) 
    aceleradora = Aceleradora.objects.get(pk=int(id_aceleradora)) 
    aceleradora.delete()

def save_inversiones(request):
    print request.POST
    profile = Startup.objects.get(user=request.user) 
    money = request.POST.get('money_inversiones', None) 
    type_invest = request.POST.get('type_inversiones', None) 
    description = request.POST.get('description_inversiones', None) 
    id_inversiones = request.POST.get('id_inversiones', None) 
    if id_inversiones!="":
        inversiones = Inversiones.objects.get(pk=int(id_inversiones)) 
        inversiones.money = money 
        inversiones.type_invest = type_invest  
        inversiones.description = description 
        inversiones.save()
    else:
        inversiones = Inversiones(money=money, type_invest=type_invest, description=description, startup=profile)
        inversiones.save()

def delete_inversiones(request):
    id_inversiones = request.POST.get('id_inversiones_delete', None) 
    inversiones = Inversiones.objects.get(pk=int(id_inversiones)) 
    inversiones.delete()

def save_transaction(request):
    profile = Startup.objects.get(user=request.user) 
    client_number = request.POST.get('client_number', None) 
    total_ingresos = request.POST.get('total_ingresos', None) 
    total_month = request.POST.get('total_month', None) 
    costo = request.POST.get('costo', None) 
    profile.client_number = client_number 
    profile.total_ingresos = total_ingresos  
    profile.total_month = total_month 
    profile.costo = costo 
    profile.save()

def save_competidores(request):
    profile = Startup.objects.get(user=request.user) 
    competidores = request.POST.get('competidores', None) 
    competidores_obj = Competidores.objects.filter(startup=profile)
    competidores_obj.delete()
    for i in range(int(competidores)):
         name_competidor = request.POST.get('name_competidor_'+str(i+1), None) 
         web = request.POST.get('web_'+str(i+1), None) 
         diferencias = request.POST.get('diferencias_'+str(i+1), None) 
         if name_competidor:
            competidores = Competidores(name=name_competidor, 
                                            web=web,
                                            diferencias=diferencias, startup=profile)
            competidores.save()    

def save_banner(request):
    id_profile = request.POST.get('id_profile', None) 
    profile = Profile.objects.get(id=id_profile)
    profile.email = request.POST.get('username', None)    
    city = request.POST.get('city', None) 
    city = city.split(' , ') 
    city = City.objects.get(name=city[0], country__name=city[1])
    user_city = UserCity.objects.filter(user=request.user)[0]
    user_city.city = city
    user_city.save()
    profile.last_name = request.POST.get('last_name', None) 
    profile.first_name = request.POST.get('name', None) 
    profile.twitter = request.POST.get('twitter', None) 
    profile.facebook = request.POST.get('facebook', None) 
    profile.blog = request.POST.get('blog', None) 
    profile.linkedin = request.POST.get('linkedin', None) 
    profile.description = request.POST.get('mini_resume', None) 
    profile.save()

def save_money_investor(request):
    profile = ProfileInvest.objects.filter(user=request.user)[0]
    profile.budget = request.POST.get('budget', None)        
    profile.money = request.POST.get('money', None)  
    profile.save()
    invest_industry = InvestIndustry.objects.filter(investor=profile)
    invest_industry.delete()
    industry = request.POST.get('industry', None) 
    industry = Industry.objects.get(id=industry)
    invest_industry = InvestIndustry(investor=profile, industry=industry)
    invest_industry.save()   


def eeaa_home(request, id_selection=None):
    template_name = 'arquitectura_eeaa/home.html'
    data = {}
    user_id = request.user.id
    cities = City.objects.all()
    countries = Country.objects.all()
    categories = Category.objects.all()
    data['id_selection'] = id_selection
    data['countries'] = countries
    data['cities'] = cities
    data['countries_list'] = countries.values_list('name', flat=True)
    data['categories'] = categories
    data['user'] = request.user
    data['user_id'] = request.user.id
    data['myfilters'] = FilterEEAA.objects.filter(user__pk=request.user.pk)
    now = datetime.datetime.now()
    data['event_after'] = Event.objects.filter(active=True, date__gte=now)
    data['event_before'] = Event.objects.filter(active=True, date__lt=now)
    if id_selection == "1":
        data['events'] = data['event_before']
    else:
        data['events'] = data['event_after']
    data['type_filter'] = Category.objects.all()
    data['places_array'] = Place.objects.all().values_list('name', flat=True)
    data['places_top'] = PlaceCompany.objects.values(
        'place__name').annotate(count=Count('place__name'))[0:3]
    coordination_array = User.objects.all().values_list(
                                'first_name', 'last_name')
    data['coordination_array'] = []
    for i in coordination_array:
        data['coordination_array'].append(i[0]+'-'+i[1])
    if request.method == 'POST':
        if 'email' in request.POST:
            email = request.POST.get('email', None)
            name = request.POST.get('name', None)
            last_name = request.POST.get('last_name', None)
            password_1 = request.POST.get('password_1', None)
            password_2 = request.POST.get('password_2', None)
            if password_1 != password_2:
                message = "Claves no coinciden"
            else:
                try:
                    if User.objects.filter(username=email):
                        message = "Usuario ya registrado"
                    else:
                        user = User.objects.create_user(username=email,
                                                        email=email,
                                                        first_name=name,
                                                        last_name=last_name,
                                                        password=password_1)
                        user = authenticate(username=email, password=password_1)
                        auth_login(request, user)
                        message = "Registro de usuario de manera exitosa"
                except:
                    message = "Error en el registro"
            data['message'] = message
            return render_to_response(template_name, data,
                                      context_instance=RequestContext(request))
        else:
            category = request.POST.get('category', None)
            name_section = request.POST.get('name_section', None)
            #country = request.POST.get('country', None)
            #city = request.POST.get('city', None)
            country = request.POST.get('countries', None)
            date = request.POST.get('date', None)
            start_time = request.POST.get('start_time', None)
            end_time = request.POST.get('end_time', None)
            place = request.POST.get('place', None)
            address = request.POST.get('address', None)
            link = request.POST.get('link', None)
            description = request.POST.get('description', None)
            if not request.user:
                message = "Debe hacer login antes de registrar un evento"
                data['message'] = message
                return render_to_response(template_name, data,
                                          context_instance=RequestContext(
                                            request
                                            ))
            try:
                user = User.objects.filter(username=request.user.username)
                if not user:
                    message = "Tiene que estar registrado para crear el evento"
                else:
                    user = user[0]
                    #city = City.objects.get(id=city)
                    date = datetime.datetime.strptime(date, '%Y/%m/%d')
                    print country
                    country = country.split(',')
                    country = country[0]
                    country = Country.objects.get(name=country)

                    if category:
                        category = Category.objects.get(pk=category)
                        event = Event(user=user, category=category,
                                        name=name_section, country=country, date=date,
                                        start_time=start_time,
                                        end_time=end_time, place=place,
                                        link=link, address=address,
                                        description=description, active=False
                                        )
                    else:
                        event = Event(user=user, name=name_section,
                                        country=country, date=date,
                                        start_time=start_time, end_time=end_time,
                                        place=place, link=link, address=address,
                                        description=description, active=False
                                        )

                    event.save()
                    message = "Evento creado correctamente, \
                                en espera de aprobación del administrador"
            except:
                message = "Error en la creación de evento contacte al \
                            administrador"
            data['message'] = message
            return render_to_response(template_name, data,
                                      context_instance=RequestContext(request))
    else:

        return render_to_response(template_name, data,
                                  context_instance=RequestContext(request))


def ajax_event(request):
    pk = request.POST.get('pk', None)
    event = Event.objects.filter(pk=pk, active=True)
    if event:
        event = event.values('id', 'name', 'city__name', 'start_time',
                             'country__name', 'place', 'end_time', 'date',
                             'link', 'category__name', 'description')[0]
        event['date'] = str(event['date'])
    else:
        event = ""
    return HttpResponse(json.dumps({'status': 'OK', 'event': event}),
                        content_type="application/json")


def ajax_attend(request):
    user_id = request.POST.get('user_id', None)
    event_id = request.POST.get('event_id', None)
    if user_id and not(user_id == 'None'):
        user = User.objects.get(id=int(user_id))
        event = Event.objects.get(id=int(event_id))
        if not Attend.objects.filter(user=user, event=event):
            attend = Attend(user=user, event=event)
            attend.save()
            message = "Registrado para asistir a este evento"
        else:
            message = "Ya tu estas registrado para éste evento"
    else:
        message = "Debe iniciar sesión primero para participar"

    return HttpResponse(json.dumps({'status': 'OK', 'message': message}),
                        content_type="application/json")


def ajax_cities(request):
    country = request.POST.get('country', None)
    if country:
        country = Country.objects.get(id=country)
        cities = City.objects.filter(country=country).values()
        return HttpResponse(json.dumps({'status': 'OK',
                                        'cities': list(cities)}),
                            content_type="application/json")
    else:
        return HttpResponse(json.dumps({'status': 'OK'}),
                            content_type="application/json")


def ajax_filter(request):
    filters = request.POST.getlist('filters[]')
    filters = json.loads(filters[0])
    print filters
    type_company = request.POST.get('id_selection', None)
    if type_company == 'None':
        q_objects = Q()
    else:
        q_objects = Q(search=int(type_company))
    for i in filters:
        key = i['value']
        value = i['label']
        if str(key) == "type":
            q_objects = q_objects & Q(type_company__name=value)
        elif key == "place":
            company = PlaceCompany.objects.filter(
                place__name=value).values_list('company__name', flat=True)
            q_objects = q_objects & Q(name__in=company)
        elif key == "industry":
            q_objects = q_objects & Q(industry__name=value)
        elif key == "tecnology":
            #value = value[1:]
            company_tecnology = TecnologyCompany.objects.filter(
                tecnology__name=value).values_list('company__name', flat=True)
            q_objects = q_objects & Q(name__in=company_tecnology)

    result = Company.objects.filter(q_objects)
    total_company = result.count()
    place_company = {}
    try:
        for i in result:
            place_company[i.name] = {'place': json.dumps(
                list(i.get_place().values_list('place__name', flat=True))
                ), 'date': i.date_joined.strftime("%d-%m-%Y")}
        result = result.values(
            'name', 'logo', 'description', 'web', 'industry__name', 'money',
            'pk')
        result = list(result)
        place_company = json.dumps(place_company)
    except Company.DoesNotExist:
        print 'Objecto no existe'
    print place_company
    return HttpResponse(json.dumps({'status': 'OK',
                                    'result': json.dumps(result),
                                    'place_company': place_company,
                                    'total_company': total_company}),
                        content_type="application/json")


def ajax_filter_eeaa(request):
    filters = request.POST.getlist('filters[]')
    filters = json.loads(filters[0])
    id_selection = request.POST.get('id_selection', None)
    now = datetime.datetime.now()
    if id_selection == 1:
        q_objects = Q(date__gte=now)
    else:
        q_objects = Q(date__lt=now)
    q_objects = q_objects & Q(active=True)
    for i in filters:
        key = i['value']
        value = i['label']
        if str(key) == "type":
            q_objects = q_objects & Q(category__name=str(value))
        elif key == "place":
            q_objects = q_objects & Q(country__name=str(value))
        elif key == "coordination":
            value_array = value.split('-')
            q_objects = q_objects & Q(user__first_name=str(value_array[0]),
                                      user__last_name=str(value_array[1]))

    try:
        result = Event.objects.filter(q_objects)
        events = []
        for i in result:
            events.append({'name': i.name, 'country__name': i.country.name,
                           'city__name': i.city.name,
                           'date': i.date.strftime("%d-%m-%Y"),
                           'start_time': i.start_time, 'end_time': i.end_time,
                           'place': i.place, 'link': i.link, 'id': i.pk})
    except Company.DoesNotExist:
        print 'Objecto no existe'
    return HttpResponse(json.dumps({'status': 'OK',
                                    'events': events}),
                        content_type="application/json")


@login_required
def ajax_save_filter(request):
    filters = request.POST.getlist('filters[]')
    filters = json.loads(filters[0])
    name = request.POST.get('name', None)
    user = request.POST.get('user', None)
    if user:
        user = User.objects.get(pk=user)
        filter_name = Filter.objects.filter(name=name, user=user)
        if filter_name:
            message = 'Nombre de filtro ya existente'
        else:
            filter_name = Filter(name=name, user=user)
            filter_name.save()
            for i in filters:
                key = i['value']
                value = i['label']
                data_filter = DataFilter(
                    name_filter=filter_name, data_filter=key,
                    type_filter=value)
                data_filter.save()
            message = 'Filtro salvado'
        filters = json.dumps(list(Filter.objects.filter(
            user=user).values('name', 'pk')))
    else:
        print 'Tiene que hacer login'
        message = 'Tiene que hacer login'

    return HttpResponse(json.dumps({'status': 'OK', 'message': message,
                                    'filters': filters}),
                        content_type="application/json")


@login_required
def ajax_save_filter_eeaa(request):
    filters = request.POST.getlist('filters[]')
    filters = json.loads(filters[0])
    name = request.POST.get('name', None)
    user = request.POST.get('user', None)
    if user:
        user = User.objects.get(pk=user)
        filter_name = FilterEEAA.objects.filter(name=name, user=user)
        if filter_name:
            message = 'Nombre de filtro ya existente'
        else:
            filter_name = FilterEEAA(name=name, user=user)
            filter_name.save()
            for i in filters:
                key = i['value']
                value = i['label']
                data_filter = DataFilterEEAA(
                    name_filter=filter_name, data_filter=key,
                    type_filter=value)
                data_filter.save()
            message = 'Filtro salvado'
        filters = json.dumps(list(FilterEEAA.objects.filter(
            user=user).values('name', 'pk')))
    else:
        print 'Tiene que hacer login'
        message = 'Tiene que hacer login'

    return HttpResponse(json.dumps({'status': 'OK', 'message': message,
                                    'filters': filters}),
                        content_type="application/json")


def ajax_filters_select(request):
    name = request.POST.get('name', None)
    user = request.POST.get('user', None)
    filters = Filter.objects.get(name=name, user=user)
    data_filter = json.dumps(list(
        DataFilter.objects.filter(
        name_filter=filters).values('type_filter', 'data_filter')))
    return HttpResponse(json.dumps({'status': 'OK',
                                    'data_filter': data_filter}),
                        content_type="application/json")


def ajax_filters_select_eeaa(request):
    name = request.POST.get('name', None)
    user = request.POST.get('user', None)
    filters = FilterEEAA.objects.get(name=name, user=user)
    data_filter = json.dumps(list(
        DataFilterEEAA.objects.filter(
        name_filter=filters).values('type_filter', 'data_filter')))
    return HttpResponse(json.dumps({'status': 'OK',
                                    'data_filter': data_filter}),
                        content_type="application/json")


def vitrina_home(request, id_selection=None):
    template_name = 'vitrina/home.html'
    if id_selection == "1":
        companies_show = Company.objects.filter(search=1)
        title = "Buscando inversionistas"
    elif id_selection == "2":
        companies_show = Company.objects.filter(search=2)
        title = "Trabajando con founderlist"
    else:
        companies_show = Company.objects.all()
        title = "Todas las compañias"
    type_filter = TypeCompany.objects.all()
    places = Place.objects.all()
    places_array = Place.objects.all().values_list('name', flat=True)
    places_top = PlaceCompany.objects.values(
        'place__name').annotate(count=Count('place__name'))[0:3]
    industries = Industry.objects.all()
    industry_array = Industry.objects.all().values_list('name', flat=True)
    tecnology = Tecnology.objects.all()
    tecnology_array = Tecnology.objects.all().values_list('name', flat=True)
    industries_top = Company.objects.values(
        'industry__name').annotate(count=Count('industry__name'))[0:6]
    tecnologies = Tecnology.objects.all()
    tecnologies_top = TecnologyCompany.objects.values(
        'tecnology__name').annotate(count=Count('tecnology__name'))[0:9]
    company = Company.objects.all()
    myfilters = Filter.objects.filter(user__pk=request.user.pk)
    inversionista = Company.objects.filter(search=1)
    founderlist = Company.objects.filter(search=2)
    return render_to_response(template_name, {
        'company': company, 'inversionista': inversionista,
        'founderlist': founderlist, 'companies_show': companies_show,
        'id_selection': id_selection, 'title': title,
        'type_filter': type_filter, 'places': places,
        'places_top': places_top, 'industries_top': industries_top,
        'industries': industries, 'tecnologies': tecnologies,
        'tecnologies_top': tecnologies_top,
        'myfilters': myfilters,
        'places_array': places_array,
        'industry_array': industry_array,
        'tecnology_array': tecnology_array,
        'id_selection': id_selection},
                              context_instance=RequestContext(request))


def template(request):
    template_name = 'index_template.html'
    return render_to_response(template_name, {},
                              context_instance=RequestContext(request))


def team(request):
    template_name = 'team.html'
    return render_to_response(template_name, {},

                              context_instance=RequestContext(request))


def how(request):
    template_name = 'how.html'
    return render_to_response(template_name, {},
                              context_instance=RequestContext(request))

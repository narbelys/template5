$(document).ready(function () {
  $(document).on("scroll", onScroll);

  $('[data-toggle="popover"]').popover({
    html:true,
    trigger : 'hover', //<--- you need a trigger other than manual
    delay: {
       show: "500",
    },
  });

  $('.save_btn').on('click', function (e) {
    $(this).parent().toggleClass('open');
  })
  $('.filter-menu-btn').on('click', function (e) {
    $(this).parent().toggleClass('open');
  })

  $('body').on('click', function (e) {
      if (!$('.save_information').is(e.target)
          && $('.save_information').has(e.target).length === 0
          && $('.open').has(e.target).length === 0
      ) {
          $('.save_information').removeClass('open');
      }
      if (!$('.box-dropdown').is(e.target)
          && $('.box-dropdown').has(e.target).length === 0
          //&& $('.open').has(e.target).length === 0
      ) {
          $('.box-dropdown').removeClass('open');
      }
      if (!$('.box-dropdown-2').is(e.target)
          && $('.box-dropdown-2').has(e.target).length === 0
          //&& $('.open').has(e.target).length === 0
      ) {
          $('.box-dropdown-2').removeClass('open');
      }
      if (!$('.box-dropdown-3').is(e.target)
          && $('.box-dropdown-3').has(e.target).length === 0
          //&& $('.open').has(e.target).length === 0
      ) {
          $('.box-dropdown-3').removeClass('open');
      }
  });

  $('#tokenfield-1').tokenfield({
    autocomplete: {
      source: [],
      delay: 100
    },
    showAutocompleteOnFocus: true,
    delimiter: [',',' ', '-', '_']
  });
  $('#tokenfield-eeaa').tokenfield({
    autocomplete: {
      source: [],
      delay: 100
    },
    showAutocompleteOnFocus: true,
    delimiter: [',',' ', '-', '_']
  });

$('#location_2').tokenfield(
    { 
   autocomplete: {
      source: countries,
      delay: 100
    },
    showAutocompleteOnFocus: true,
    delimiter: [',',' ', '-', '_']
  }
  );

  $('#location_3').tokenfield({
    autocomplete: {
      source: countries,
      delay: 100,
    },
    showAutocompleteOnFocus: true,
    delimiter: [',',' ', '-', '_']
  });





  //smoothscroll
  /*$('a[href^="#"]').on('click', function (e) {
      e.preventDefault();
      $(document).off("scroll");

      $('a').each(function () {
          $(this).removeClass('active');
      })
      $(this).addClass('active');

      var target = this.hash,
          menu = target;
      $target = $(target);
      $('html, body').stop().animate({
          'scrollTop': $target.offset().top+2
      }, 500, 'swing', function () {
          window.location.hash = target;
          $(document).on("scroll", onScroll);
      });
  });*/
});

function onScroll(event){
  var scrollPos = $(document).scrollTop() + $(".sticky-section").height();
  $('#menu-center a').each(function () {
      var currLink = $(this);
      var refElement = $(currLink.attr("href"));
      if (refElement.position()){
        if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
            $('#menu-center ul li a').removeClass("active");
            currLink.addClass("active");
        }
        else{
            currLink.removeClass("active");
        }
      }
  });
}

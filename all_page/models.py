from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.contrib.auth.models import User
from django.utils import timezone
import os

class Country(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name


class City(models.Model):
    name = models.CharField(max_length=200)
    country = models.ForeignKey(
        Country, help_text=_("country")
    )

    def __unicode__(self):
        return self.name

    def get_all_name(self):
        return self.name + ' , ' + self.country.name

class UserCity(models.Model):
    user = models.ForeignKey(User, default=0)
    city = models.ForeignKey(City, default=0)

    def __unicode__(self):
        return self.user.email + self.city.name

class Profile(models.Model):

    user = models.ForeignKey(User, default=0)
    step_register = models.IntegerField(
                verbose_name=_("step register"),
                help_text=_("step register"),
                blank=True, null=True
            )
    foto = models.ImageField(
                #upload_to='/home/augustoa/webapps/template_5_static/',
                upload_to='/home/ubuntu/template5_static/static',
                #upload_to='/home/vasily/Documentos/template5/all_page/static/images',
                verbose_name='My Photo', blank=True, null=True)
    description = models.CharField(
        _('description'),
        max_length=1500,
        blank=True, null=True
    )
    privacity = models.BooleanField(default=True)
    INFORMATION_CHOICES = (
        (1, "Startup"),
        (2, "Inversionista"),
    )

    type_profile = models.CharField(
        verbose_name=_("Type profile"),
        max_length=1,
        choices=INFORMATION_CHOICES,
        help_text=_("Type of the profile")
    )
    linkedin = models.CharField(
        _('linkedin'),
        max_length=1500,
        blank=True, null=True
    )    
    twitter = models.CharField(
        _('twitter'),
        max_length=1500,
        blank=True, null=True
    )    
    facebook = models.CharField(
        _('facebook'),
        max_length=1500,
        blank=True, null=True
    )    
    blog = models.CharField(
        _('blog'),
        max_length=1500,
        blank=True, null=True
    )

    def __unicode__(self):
        return self.user.username

    def name_photo(self):
        return os.path.basename(self.foto.name)

class Startup(models.Model):

    user = models.ForeignKey(User, default=0)
    foto = models.ImageField(
                #upload_to='/home/augustoa/webapps/template_5_static/',
                upload_to='/home/ubuntu/template5_static/static',
                #upload_to='/home/vasily/Documentos/template5/all_page/static/images',
                verbose_name='My Photo', blank=True, null=True)

    name_index = models.CharField(
        _(' name index'),
        max_length=1500,
        blank=True, null=True
    )     
    name = models.CharField(
        _(' name'),
        max_length=1500,
        blank=True, null=True
    )       
    type_bussines = models.CharField(
        _(' type_bussines'),
        max_length=1500,
        blank=True, null=True
    )    
    description = models.CharField(
        _('description'),
        max_length=1500,
        blank=True, null=True
    )    
    who_are = models.CharField(
        _('quienes somos'),
        max_length=1500,
        blank=True, null=True
    )    
    why = models.CharField(
        _('Por que estamos haciendo esto'),
        max_length=1500,
        blank=True, null=True
    )
    ingreso_model = models.CharField(
        _('modelo de ingreso'),
        max_length=1500,
        blank=True, null=True
    )    
    video = models.CharField(
        _('video'),
        max_length=150,
        blank=True, null=True
    )
    client_number = models.IntegerField(
                verbose_name=_("numero de clientes"),
                help_text=_("numero de clientes"),
                blank=True, null=True
            )     
    total_ingresos = models.IntegerField(
                verbose_name=_("ingresos"),
                help_text=_("ingresos"),
                blank=True, null=True
            )     
    total_month = models.IntegerField(
                verbose_name=_("total mensual"),
                help_text=_("total mensual"),
                blank=True, null=True
            )     
    costo = models.IntegerField(
                verbose_name=_("costo"),
                help_text=_("costo"),
                blank=True, null=True
            ) 
    chanel_distribution = models.CharField(
        _('canales de distribucion'),
        max_length=1500,
        blank=True, null=True
    )    
    mercado = models.CharField(
        _('mercado'),
        max_length=1500,
        blank=True, null=True
    )
    city = models.ForeignKey(City, default=0)
    linkedin = models.CharField(
        _('linkedin'),
        max_length=1500,
        blank=True, null=True
    )    
    twitter = models.CharField(
        _('twitter'),
        max_length=1500,
        blank=True, null=True
    )    
    facebook = models.CharField(
        _('facebook'),
        max_length=1500,
        blank=True, null=True
    )    
    blog = models.CharField(
        _('blog'),
        max_length=1500,
        blank=True, null=True
    )    
    web_page = models.CharField(
        _('web page'),
        max_length=1500,
        blank=True, null=True
    )
    INFORMATION_CHOICES = (
        ("1", "1-10"),
        ("2", "10-20"),        
        ("3", "20-50"),
    )

    employee = models.CharField(
        verbose_name=_("Type employee"),
        max_length=1,
        choices=INFORMATION_CHOICES,
        help_text=_("Type of the employee")
    ) 

    def __unicode__(self):
        return self.name

    def get_video(self):
        name_video = self.video.split('youtube.com')
        return name_video[0]+"youtube.com/embed/" + name_video[1]

class UserStartup(models.Model):
    startup = models.ForeignKey(Startup, default=0)
    user = models.ForeignKey(User, default=0)
    date_joined = models.DateTimeField(
        _('date joined'), default=timezone.now,
        help_text=_("Date joined"),
    )

class Ronda(models.Model):
    startup = models.ForeignKey(Startup, default=0)
    size = models.IntegerField(
                verbose_name=_("tamanio de la ronda"),
                help_text=_("tamanio de la ronda"),
                blank=True, null=True
            )    
    search_money = models.IntegerField(
                verbose_name=_("dinero buscado "),
                help_text=_("dinero buscado"),
                blank=True, null=True
            )    
    participacion = models.IntegerField(
                verbose_name=_("participacion "),
                help_text=_("participacion"),
                blank=True, null=True
            )    
    valorizacion = models.IntegerField(
                verbose_name=_("valorizacion"),
                help_text=_("valorizacion"),
                blank=True, null=True
            )
    other_term = models.CharField(
        _('otros terminos'),
        max_length=1500,
        blank=True, null=True
    )
    equity = models.BooleanField(default=True)

    def __unicode__(self):
        return self.startup.name

class Capitalizacion(models.Model):
    ronda = models.ForeignKey(Ronda, default=0)
    name_capitalizacion = models.CharField(
        _('name'), max_length=100,
        help_text=_("name"),
    )
    INFORMATION_CHOICES = (
        ("1", "Fundador"),
        ("2", "Co-fundador"),        
    )

    type_paticipacion = models.CharField(
        verbose_name=_("Type participacion"),
        max_length=1,
        choices=INFORMATION_CHOICES,
        help_text=_("Type participacion")
    ) 

    participacion = models.IntegerField(
                verbose_name=_("participacion"),
                help_text=_("participacion"),
                blank=True, null=True
            )
    def __unicode__(self):
        return self.name_capitalizacion

class Hitos(models.Model):
    ronda = models.ForeignKey(Ronda, default=0)
    hitos = models.CharField(
        _('hitos'), max_length=100,
        help_text=_("hitos"),
    )
    activities = models.CharField(
        _('actividades'), max_length=100,
        help_text=_("actividades"),
    )          
    time = models.CharField(
        _('tiempo'), max_length=100,
        help_text=_("tiempo"),
    )
    money = models.IntegerField(
                verbose_name=_("participacion"),
                help_text=_("participacion"),
                blank=True, null=True
            )   
    def __unicode__(self):
        return self.hitos

class Product(models.Model):
    startup = models.ForeignKey(Startup, default=0)
    video = models.CharField(
        _('video'),
        max_length=150,
        blank=True, null=True
    ) 
    description = models.CharField(
        _('description'),
        max_length=1500,
        blank=True, null=True
    ) 
    def __unicode__(self):
        return self.video    

    def get_video(self):
        name_video = self.video.split('youtube.com')
        return name_video[0]+"youtube.com/embed/" + name_video[1]

class Fundadores(models.Model):
    startup = models.ForeignKey(Startup, default=0)
    name = models.CharField(
        _('description'),
        max_length=150,
        blank=True, null=True
    )    
    cargo = models.CharField(
        _('cargo'),
        max_length=150,
        blank=True, null=True
    )  
    INFORMATION_CHOICES = (
        ("1", "Full-time"),
        ("2", "Part-time"),        
        ("3", "No dedica horas"),        
    )

    tiempo = models.CharField(
        verbose_name=_("tiempo dedicado"),
        max_length=1,
        choices=INFORMATION_CHOICES,
        help_text=_("tiempo dedicado"),
        blank=True, null=True
    ) 
    description = models.CharField(
        _('description'),
        max_length=1500,
        blank=True, null=True
    )   

class Clientes(models.Model):
    startup = models.ForeignKey(Startup, default=0)
    name = models.CharField(
        _('description'),
        max_length=150,
        blank=True, null=True
    )    
    contacto = models.CharField(
        _('contacto'),
        max_length=150,
        blank=True, null=True
    )  
    description = models.CharField(
        _('description'),
        max_length=1500,
        blank=True, null=True
    )   

class Aliados(models.Model):
    startup = models.ForeignKey(Startup, default=0)
    name = models.CharField(
        _('description'),
        max_length=150,
        blank=True, null=True
    )
    contacto = models.CharField(
        _('contacto'),
        max_length=150,
        blank=True, null=True
    )       
    description = models.CharField(
        _('description'),
        max_length=1500,
        blank=True, null=True
    )   

class EmployeeAll(models.Model):
    startup = models.ForeignKey(Startup, default=0)
    name = models.CharField(
        _('description'),
        max_length=150,
        blank=True, null=True
    )
    contacto = models.CharField(
        _('contacto'),
        max_length=150,
        blank=True, null=True
    )       
    description = models.CharField(
        _('description'),
        max_length=1500,
        blank=True, null=True
    )  

class Directory(models.Model):
    startup = models.ForeignKey(Startup, default=0)
    name = models.CharField(
        _('description'),
        max_length=150,
        blank=True, null=True
    )
    contacto = models.CharField(
        _('contacto'),
        max_length=150,
        blank=True, null=True
    )       
    description = models.CharField(
        _('description'),
        max_length=1500,
        blank=True, null=True
    )  

class Advisor(models.Model):
    startup = models.ForeignKey(Startup, default=0)
    name = models.CharField(
        _('description'),
        max_length=150,
        blank=True, null=True
    )
    contacto = models.CharField(
        _('contacto'),
        max_length=150,
        blank=True, null=True
    )       
    description = models.CharField(
        _('description'),
        max_length=1500,
        blank=True, null=True
    ) 

class Inversiones(models.Model):
    startup = models.ForeignKey(Startup, default=0)
    money = models.CharField(
        _('description'),
        max_length=150,
        blank=True, null=True
    )
    type_invest = models.CharField(
        _('tipo de inversion'),
        max_length=150,
        blank=True, null=True
    )       
    description = models.CharField(
        _('description'),
        max_length=1500,
        blank=True, null=True
    ) 

class Aceleradora(models.Model):
    startup = models.ForeignKey(Startup, default=0)
    name = models.CharField(
        _('description'),
        max_length=150,
        blank=True, null=True
    )
    contacto = models.CharField(
        _('contacto'),
        max_length=150,
        blank=True, null=True
    )       
    description = models.CharField(
        _('description'),
        max_length=1500,
        blank=True, null=True
    )   
class Competidores(models.Model):
    startup = models.ForeignKey(Startup, default=0)
    name = models.CharField(
        _('description'),
        max_length=150,
        blank=True, null=True
    )     
    web = models.CharField(
        _('web'),
        max_length=1500,
        blank=True, null=True
    )      
    diferencias = models.CharField(
        _('diferencias'),
        max_length=1500,
        blank=True, null=True
    )    


class Industry(models.Model):

    name = models.CharField(
        _('name'), max_length=100,
        help_text=_("name"),
    )

    def __unicode__(self):
        return self.name


class ProfileInvest(models.Model):
    user = models.ForeignKey(User, default=0)
    INFORMATION_CHOICES = (
        ("1", "USD $1k-4k"),
        ("2", "USD $4k-8k"),
        ("3", "USD $8k-12k"),
    )
    INFORMATION_CHOICES_MONEY = (
        ("1", "USD $1k-2k"),
        ("2", "USD $2k-4k"),
        ("3", "USD $4k-8k"),
    )
    budget = models.CharField(
        verbose_name=_("Money"),
        max_length=1,
        choices=INFORMATION_CHOICES,
    )
    money = models.CharField(
        verbose_name=_("Type profile"),
        max_length=1,
        choices=INFORMATION_CHOICES_MONEY,
        help_text=_("Type of the profile")
    )


class InvestIndustry(models.Model):
    investor = models.ForeignKey(
        ProfileInvest, help_text=_("investor")
    )
    industry = models.ForeignKey(
        Industry, help_text=_("industry")
    )

class Category(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name


class UserInterest(models.Model):
    user = models.ForeignKey(User, default=0)
    interest = models.CharField(
        _('Interes'), max_length=1500, help_text=_("Interes"),
    )
    type_interest = models.CharField(
        _('tipo de interes'), max_length=1500, help_text=_("tipo de interes"),
    )

    def __unicode__(self):
        return self.user.email + self.interest


class UserTypeInvest(models.Model):
    user = models.ForeignKey(User, default=0)
    INFORMATION_CHOICES = (
        ("1", "Individuo"),
        ("2", "Fondo de inversion"),
        ("3", "Family Office"),
        ("4", "Empresa"),
    )

    type_invest = models.CharField(
        verbose_name=_("Type invest"),
        max_length=1,
        choices=INFORMATION_CHOICES,
        help_text=_("Type of the invest")
    )

    def __unicode__(self):
        return self.user.email + self.type_invest


class ValueToStartup(models.Model):
    user = models.ForeignKey(User, default=0)
    INFORMATION_CHOICES = (
        (1, "Directamente en una Startup"),
        (2, "Directamente desde mi empresa"),
        (3, "Desde un Family Office"),
        (4, "No he invertido anteriormente"),
    )

    value_to_startup = models.CharField(
        verbose_name=_("Value To Startup"),
        max_length=1,
        choices=INFORMATION_CHOICES,
        help_text=_("Value To Startup")
    )

    def __unicode__(self):
        return self.user.email    



class PreviouslyInvested(models.Model):
    user = models.ForeignKey(User, default=0)
    INFORMATION_CHOICES = (
        (1, "Conocimiento en alguna industria en particular"),
        (2, "Conexion con clientes y/o afiliados"),
        (3, "Family Office"),
        (4, "Direccion estrategica del negocio"),
        (5, "Otros"),
    )

    previously_invested = models.CharField(
        verbose_name=_("Previousy Invested"),
        max_length=1,
        choices=INFORMATION_CHOICES,
        help_text=_("Previously Invested")
    )

    other = models.CharField(
        _('other'), max_length=1500, help_text=_("other"),
    )

    def __unicode__(self):
        return self.user.email


class UserAcreditation(models.Model):
    user = models.ForeignKey(User, default=0)
    investment_fund = models.CharField(
        _('name'), max_length=1500, help_text=_("name"),
        blank=True, null=True
    )
    name_contact = models.CharField(
        _('name contact'), max_length=1500, help_text=_("name contact"),
        blank=True, null=True
    )
    contact_name_founderlist = models.CharField(
        _('contact name founderlist'), max_length=1500,
        help_text=_("contact name founderlist"),
        blank=True, null=True
    )
    contact_founderlist = models.BooleanField(default=False)
    web = models.CharField(
        _('web'), max_length=1500, help_text=_("web"),
        blank=True, null=True
    )

    def __unicode__(self):
        return self.user.email


class Event(models.Model):

    user = models.ForeignKey(User, default=0)
    category = models.ForeignKey(
        Category, help_text=_("category"), null=True, blank=True
    )
    name = models.CharField(
        _('Nombre'), max_length=1500, help_text=_("name"),
    )
    country = models.ForeignKey(
        Country, help_text=_("country")
    )
    city = models.ForeignKey(
        City, help_text=_("city"), null=True, blank=True
    )
    date = models.DateField(
        _('Date'), help_text=_("date"),
    )
    start_time = models.CharField(
        _('start_time'), max_length=1500, help_text=_("start_time"),
    )
    end_time = models.CharField(
        _('end_time'), max_length=1500, help_text=_("end_time"),
        null=True, blank=True
    )
    place = models.CharField(
        _('place'), max_length=1500, help_text=_("place"),
        null=True, blank=True
    )
    address = models.CharField(
        _('address'), max_length=1500, help_text=_("address"),
        null=True, blank=True
    )
    link = models.CharField(
        _('link'), max_length=1500, help_text=_("link"),
        null=True, blank=True
    )
    description = models.CharField(
        _('description'), max_length=1500, help_text=_("description"),
        null=True, blank=True
    )
    active = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name

class EventCountry(models.Model):
    country = models.ForeignKey(
        Country, help_text=_("country")
    )
    event = models.ForeignKey(
        Event, help_text=_("event"), blank=True, null=True,
    )

# Create your models here.
class Survey(models.Model):
    """
    Survey model

    """

    # required fields
    name = models.CharField(
        _('Nombre'), max_length=1500, help_text=_("name"),
    )
    last_name = models.CharField(
        _('Apellido'), max_length=1500, help_text=_("last name"),
    )
    mail = models.CharField(
        _('mail'), max_length=100,
        unique=True, help_text=_("mail"),
    )
    web = models.CharField(
        _('web'), max_length=1500, blank=True, null=True,
        help_text=_("web"),
    )
    sell_service = models.NullBooleanField(
        _('Ofrece servicio'), blank=True, null=True,
        help_text=_("sell service"),
    )
    sell_product = models.NullBooleanField(
        _('Ofrece producto'), blank=True, null=True,
        help_text=_("sell product"),
    )
    invest = models.NullBooleanField(
        _('Aprende a invertir'), blank=True, null=True, help_text=_("invest"),
    )
    bci = models.NullBooleanField(
        _('Cliente BCI'), blank=True, null=True, help_text=_("client of BCI"),
    )
    phone = models.CharField(
            _('Phone'), max_length=50, help_text=_("phone"),
            null=True, blank=True
    )

    def __unicode__(self):
        return self.name + ' ' + self.last_name


# Create your models here.
class Information(models.Model):
    """
    Information model

    """
    INFORMATION_CHOICES = (
        (1, "Ejecutivo"),
        (2, "Mail"),
        (3, "Facebook"),
        (4, "Twitter"),
        (5, "Referencia"),
        (6, "Otra")
    )

    survey = models.ForeignKey(
        Survey, help_text=_("survey")
    )
    origin = models.CharField(
        verbose_name=_("Type"),
        max_length=1,
        choices=INFORMATION_CHOICES,
        help_text=_("Type of the option")
    )

    def __unicode__(self):
        return self.survey.mail


# Create your models here.
class SellService(models.Model):
    """
    Information model

    """
    CHOICES = (
        (1, _("Contaduria")),
        (2, _("Coaching")),
        (3, _("Disenio")),
        (4, _("Catering")),
        (5, _("Marketing")),
        (6, _("Desarrollo de software")),
        (7, _("Servicio al cliente")),
        (8, _("Recursos humanos")),
        (9, _("Legal")),
        (10, _("Ventas")),
        (11, _("Otro"))
    )

    survey = models.ForeignKey(
        Survey, help_text=_("surveyl")
    )
    sell_type = models.CharField(
        verbose_name=_("sell_type"),
        max_length=1,
        choices=CHOICES,
        help_text=_("Type of the option")
    )


class Attend(models.Model):
    user = models.ForeignKey(User, default=0)
    event = models.ForeignKey(
        Event, help_text=_("event"), blank=True, null=True,
    )

    def __unicode__(self):
        return self.user.email+' - ' + self.event.name

# ------------Vitrina-----------


class TypeCompany(models.Model):

    name = models.CharField(
        _('name'), max_length=100,
        help_text=_("name"),
    )

    def __unicode__(self):
        return self.name


class Company(models.Model):

    INFORMATION_CHOICES = (
        (1, _("Buscando inversionista")),
        (2, _("Trabajando con FounderList"))
    )
    logo = models.ImageField(
        upload_to='/home/augustoa/webapps/template_5_static/images',
        verbose_name='Logo', blank=True)
    name = models.CharField(
        _('name'), max_length=100,
        help_text=_("name"),
    )
    description = models.TextField(
        _('description'), max_length=1500,
        help_text=_("description"),
    )
    web = models.CharField(
        _('web'), max_length=1500,
        help_text=_("web"),
    )
    type_company = models.ForeignKey(TypeCompany)
    industry = models.ForeignKey(Industry)
    search = models.IntegerField(
        verbose_name=_("search"),
        choices=INFORMATION_CHOICES,
        help_text=_("Type of the option")
    )
    money = models.FloatField(
        verbose_name=_("money"),
        help_text=_("Money"),
        default=0
    )
    date_joined = models.DateTimeField(
        _('date joined'), default=timezone.now,
        help_text=_("Date joined"),
    )

    def __unicode__(self):
        return self.name

    def get_place(self):
        places = PlaceCompany.objects.filter(company=self)
        return places


class Place(models.Model):

    name = models.CharField(
        _('name'), max_length=100,
        help_text=_("name"),
    )

    def __unicode__(self):
        return self.name


class PlaceCompany(models.Model):
    company = models.ForeignKey(Company)
    place = models.ForeignKey(Place)

    def __unicode__(self):
        return self.company.name+' - ' + self.place.name


class Tecnology(models.Model):

    name = models.CharField(
        _('name'), max_length=100,
        help_text=_("name"),
    )

    def __unicode__(self):
        return self.name


class TecnologyCompany(models.Model):
    company = models.ForeignKey(Company)
    tecnology = models.ForeignKey(Tecnology)

    def __unicode__(self):
        return self.company.name+' - ' + self.tecnology.name


class Filter(models.Model):
    user = models.ForeignKey(User)
    name = models.CharField(
        _('name'), max_length=100,
        help_text=_("name"),
    )

    def __unicode__(self):
        return self.name


class DataFilter(models.Model):
    INFORMATION_FILTER = (
        (1, "Tipo"),
        (2, "Locacion"),
        (3, "Industria"),
        (4, "Tecnologia"),
        (5, "Equipo")
    )
    name_filter = models.ForeignKey(Filter)
    data_filter = models.CharField(
        _('data filter'), max_length=100,
        help_text=_("data filter"),
    )
    type_filter = models.CharField(
        _('type filter'), max_length=100,
        help_text=_("type filter"),
    )

    def __unicode__(self):
        return self.name_filter.name


class FilterEEAA(models.Model):
    user = models.ForeignKey(User)
    name = models.CharField(
        _('name'), max_length=100,
        help_text=_("name"),
    )

    def __unicode__(self):
        return self.name


class DataFilterEEAA(models.Model):
    INFORMATION_FILTER = (
        (1, "Tipo"),
        (2, "Locacion"),
        (3, "Coordination"),
    )
    name_filter = models.ForeignKey(FilterEEAA)
    data_filter = models.CharField(
        _('data filter'), max_length=100,
        help_text=_("data filter"),
    )
    type_filter = models.CharField(
        _('type filter'), max_length=100,
        help_text=_("type filter"),
    )

    def __unicode__(self):
        return self.name_filter.name

class Investments(models.Model):
    user = models.ForeignKey(
        User, help_text=_("user")
    )

    name = models.CharField(
        _('name'), max_length=100,
        help_text=_("name"),
    )

    year = models.CharField(
        _('name'), max_length=100,
        help_text=_("name"),
    )    

    stage = models.CharField(
        _('name'), max_length=100,
        help_text=_("name"),
    )